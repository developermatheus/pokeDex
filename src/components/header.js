import React from 'react';
import { Text, View } from 'react-native';

const Header = (props) => {
    const { textoHeader, estiloView } = estilos;
    return (
        <View style={estiloView}>
            <Text style={textoHeader}>{props.tituloHeader}</Text>
        </View>
    );
};

const estilos = {

    estiloView: {
        backgroundColor: '#ccc',
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
        // paddingTop: 15
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.2, //for iOS
        elevation: 10, //for Android
        position: 'relative'
    },

    textoHeader: {
        fontSize: 20,
    }

}

export default Header;
