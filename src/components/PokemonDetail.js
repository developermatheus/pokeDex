import React from 'react';
import { View, Text } from 'react-native';

const PokemonDetail = ({ pokemon }) => {
    const { name } = pokemon;
    const { caixa, caixaIndividual } = estilos;

    return (
        <View style={caixa}>
            <View style={caixaIndividual}>
                <Text>
                    {name}
                </Text>
            </View>
        </View>
    )
}

const estilos = {

    caixa: {
        marginTop:2,
        padding: 2
    },

    caixaIndividual: {
        margin: 2,
        height: 100,
        // width: Dimensions.get('window').width / 3 -6,
        width: 100,
        borderRadius: 100/2,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f1c40f'
    },
    
};

export default PokemonDetail;
