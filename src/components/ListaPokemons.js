import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import axios from 'axios';
import PokemonDetail from './PokemonDetail';

class ListaPokemons extends Component {

    state = { pokemons: [] };

    componentWillMount() {
        console.log('teste de componentWillMount');
        axios.get('https://pokeapi.co/api/v2/pokemon/')
            .then(response => this.setState({ pokemons: response.data.results }));
            // .then(response => console.log(response.data));
            console.log(this.state.pokemons);
    }

    renderPokemons() {
        return this.state.pokemons.map(pokemon =>
            <PokemonDetail key={pokemon.name} pokemon={pokemon} />);
    }

    render() {

        console.log(this.state);

        const { caixaListaPokemons } = estilos;
        
        return(
            <ScrollView>
                <View style={caixaListaPokemons}>
                    { this.renderPokemons() }
                </View>
            </ScrollView>
        );
    }
}

const estilos = {
    caixaListaPokemons: {
        flexWrap: 'wrap',
        flexDirection:'row',
        justifyContent: 'center'
    }
}

export default ListaPokemons;