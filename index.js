import React  from 'react';
import { AppRegistry, View } from 'react-native';
import Header from './src/components/header';
import ListaPokemons from './src/components/ListaPokemons';

const App = () => (
        <View style={{ flex: 1 }}>
            <Header tituloHeader={'PokeDex'} />
            <ListaPokemons />
        </View>
    );

AppRegistry.registerComponent('pokeDex', () => App);
